# Clear Score Task Readme


## Project Setup

Download the project from git and open in Android Studio. Sync and the project and run.

To install the app from the command line:
```
./gradlew installDebug
```

To clean and rebuild the project:
```
./gradlew clean assemble
```

## Running tests

To run unit tests:
```
./gradlew test
```

To run instrumentation tests:
```
./gradlew connectedAndroidTest
```

## Third Party Libraries

**Retrofit** - Used for networking & converting the JSON response into models.

**RxJava** - Used for asynchronous tasks

### Testing

**Mockk** - As Kotlin files are final by default, Mockito cannot be used unless classes are declared open. However, with
MockK we do not need to alter our implementation to mock out classes.

**Robolectric** - Used for testing Android specific functionality