package com.safyu.clearscoretask.model

import com.google.gson.annotations.SerializedName

/**
 * Represents ths Credit Report model.
 *
 * Created by Safyu on 09/02/2018.
 */
data class CreditReportInfo (
        @SerializedName("score") val score: Int
)