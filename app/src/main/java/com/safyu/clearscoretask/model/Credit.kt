package com.safyu.clearscoretask.model

import com.google.gson.annotations.SerializedName

/**
 * Represents ths Credit API call data model.
 *
 * Created by Safyu on 09/02/2018.
 */
data class Credit (
        @SerializedName("creditReportInfo") val creditReportInfo: CreditReportInfo
)
