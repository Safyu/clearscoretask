package com.safyu.clearscoretask.networking

import com.safyu.clearscoretask.model.Credit
import io.reactivex.Flowable
import retrofit2.http.GET

/**
 * Describes the API calls for the Clear Score Service.
 *
 * Created by Safyu on 09/02/2018.
 */
interface ClearScoreService {

    @GET("prod/mockcredit/values")
    fun search(): Flowable<Credit>
}
