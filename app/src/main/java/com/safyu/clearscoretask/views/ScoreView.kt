package com.safyu.clearscoretask.views

/**
 * Describes the view for displaying a credit score.
 *
 * Created by Safyu on 10/02/2018.
 */
interface ScoreView {

    /**
     * Prompts the view to display the provided credit score.
     *
     * @param score The score to display.
     */
    fun updateScore(score: Int)

    /**
     * Displays a generic error to the user.
     */
    fun displayError()

    /**
     * Displays an error informing the user they are not connected to the internet.
     */
    fun displayNotConnectedError();

    /**
     * Shows the loading progress bar and text.
     */
    fun showLoading()
}