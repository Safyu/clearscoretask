package com.safyu.clearscoretask.util

import android.content.Context
import android.net.ConnectivityManager

/**
 * Utility class for checking the network connectivity.
 *
 * Created by Safyu on 10/02/2018.
 */
class NetworkConnectivityChecker (private var context : Context) {

    /**
     * Checks whether the device is connected to the network or not.
     *
     * @return True if the device is connected to a network, else false.
     */
    fun isNetworkConnected() : Boolean {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val connection = manager.activeNetworkInfo

        return (connection != null && connection.isConnectedOrConnecting)
    }

}