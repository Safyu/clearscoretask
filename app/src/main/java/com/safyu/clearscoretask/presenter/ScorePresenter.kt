package com.safyu.clearscoretask.presenter

import com.safyu.clearscoretask.interactor.ScoreInteractor
import com.safyu.clearscoretask.model.Credit
import com.safyu.clearscoretask.util.NetworkConnectivityChecker
import com.safyu.clearscoretask.views.ScoreView
import io.reactivex.functions.Consumer

/**
 * Presenter containing the business logic for displaying a credit score.
 *
 * @param scoreInteractor The interactor for obtaining the score from the network.
 *
 * Created by Safyu on 09/02/2018.
 */
class ScorePresenter(private val scoreInteractor: ScoreInteractor,
                     private val networkConnectivityChecker: NetworkConnectivityChecker) {

    /**
     * Handles the launch of the view.
     *
     * @param score The currently obtained credit score. A score over 0 is valid, else a network call will be performed.
     * @param scoreView The view to call back on.
     */
    fun handleLaunch(score: Int, scoreView: ScoreView) {
        if (score >= 0) {
            // Since we already have a valid score, we don't need to fetch it from the server.
            scoreView.updateScore(score)
        } else {
            performScoreRequestIfConnectedElseShowError(scoreView)
        }
    }

    /**
     * Handles what should happen when onDestroy is called.
     */
    fun handleOnDestroy() {
        scoreInteractor.removeObservers();
    }

    /**
     * Handles the behaviour when the retry button is pressed.
     *
     * @param scoreView The view to call back on.
     */
    fun handleScoreRetry(scoreView: ScoreView) {
        scoreView.showLoading()

        performScoreRequestIfConnectedElseShowError(scoreView)
    }

    /**
     * Performs the network request for the score.
     *
     * @param scoreView The view to call back on.
     */
    private fun performNetworkRequestForScore(scoreView: ScoreView) {
        scoreInteractor.performScoreLookup(Consumer<Credit> {
            credit ->
            // Even though the request was successful, we cannot guarantee we have the required data
            if (credit?.creditReportInfo?.score != null) {
                scoreView.updateScore(credit.creditReportInfo.score)
            } else {
                scoreView.displayError();
            }
        }, Consumer<Throwable> {
            scoreView.displayError();
        })
    }

    /**
     * Performs the score request if the device is connected, else it will call back onto the view to show an error.
     *
     * @param scoreView The view to call back on.
     */
    private fun performScoreRequestIfConnectedElseShowError(scoreView: ScoreView) {
        if (networkConnectivityChecker.isNetworkConnected()) {
            performNetworkRequestForScore(scoreView)
        } else {
            scoreView.displayNotConnectedError();
        }
    }
}
