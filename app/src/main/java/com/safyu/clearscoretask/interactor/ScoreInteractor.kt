package com.safyu.clearscoretask.interactor

import com.safyu.clearscoretask.model.Credit
import com.safyu.clearscoretask.networking.ClearScoreService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

/**
 * Interactor for handling networking calls for the Score screen.
 *
 * Created by Safyu on 09/02/2018.
 */
class ScoreInteractor (private val clearScoreService: ClearScoreService) {

    /** Container containing objects (Observables, etc) that can be easily disposed of if so needed. */
    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    /**
     * Performs the network request of looking up a credit score.
     *
     * @param onSuccess The code that should be run when the network call has completed successfully.
     * @param onFailure The code that should be run when the network call has resulted in a failure.
     */
    fun performScoreLookup(onSuccess: Consumer<in Credit>, onFailure: Consumer<in Throwable>) {
        compositeDisposable.add(
                clearScoreService.search()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe (onSuccess, onFailure)
        )
    }

    /**
     * Removes any observers that may be subscribed to observables.
     */
    fun removeObservers() {
        compositeDisposable.clear()
    }

}
