package com.safyu.clearscoretask.factory

import android.content.Context
import com.safyu.clearscoretask.presenter.ScorePresenter
import com.safyu.clearscoretask.util.NetworkConnectivityChecker

/**
 * Factory for providing lazy instantiated presenters.
 *
 * @param interactorFactory The factory for obtaining interactor instances.
 * @param context The application's context. Used for utility functionality.
 *
 * Created by Safyu on 09/02/2018.
 */
class PresenterFactory(private val interactorFactory: InteractorFactory, private val context: Context) {

    /**
     * The instance of the [ScorePresenter] for handling the score screen's business logic.
     */
    val scorePresenter: ScorePresenter by lazy {
        ScorePresenter(interactorFactory.scoreInteractor, NetworkConnectivityChecker(context))
    }
}
