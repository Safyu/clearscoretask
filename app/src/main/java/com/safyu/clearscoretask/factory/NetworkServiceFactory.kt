package com.safyu.clearscoretask.factory

import com.safyu.clearscoretask.networking.ClearScoreService

/**
 * Factory for providing network service instances.
 *
 * @param clearScoreServiceUrl The URL that the Clear Score Service should call.
 *
 * Created by Safyu on 10/02/2018.
 */
class NetworkServiceFactory(clearScoreServiceUrl : String) {

    /**
     * The instance of the [ClearScoreService] for performing networking tasks against the Clear Score server.
     */
    val clearScoreService: ClearScoreService by lazy {
        val retrofit = retrofit2.Retrofit.Builder()
                .addCallAdapterFactory(retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory.create())
                .addConverterFactory(retrofit2.converter.gson.GsonConverterFactory.create())
                .baseUrl(clearScoreServiceUrl)
                .build()

        retrofit.create(ClearScoreService::class.java);
    }
}





