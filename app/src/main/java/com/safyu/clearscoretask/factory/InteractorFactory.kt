package com.safyu.clearscoretask.factory

import com.safyu.clearscoretask.interactor.ScoreInteractor

/**
 * Factory for providing lazy instantiated interactors.
 *
 * @param networkServiceFactory The factory for obtaining network service instances.
 *
 * Created by Safyu on 09/02/2018.
 */
class InteractorFactory (networkServiceFactory: NetworkServiceFactory) {

    /**
     * The instance of the [ScoreInteractor] for performing networking and database tasks.
     */
    val scoreInteractor: ScoreInteractor by lazy {
        ScoreInteractor(networkServiceFactory.clearScoreService)
    }
}
