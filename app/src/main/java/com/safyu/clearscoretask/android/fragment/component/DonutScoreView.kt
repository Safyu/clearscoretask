package com.safyu.clearscoretask.android.fragment.component

import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import com.safyu.clearscoretask.R

/**
 * Custom view for displaying a credit score.
 *
 * Created by Safyu on 10/02/2018.
 */
class DonutScoreView : RelativeLayout {

    /** Represents the maximum credit score value. */
    private val MAX_CREDIT_SCORE = 700

    /** Progress bar that displays a graphical representation of the credit score. */
    private lateinit var _scoreProgressBar: ProgressBar

    /** [TextView] for holding the actual value of the credit score. */
    private lateinit var _creditScoreTextView: TextView

    /**
     * Creates a new instance.
     *
     * @param context Used for inflation.
     */
    constructor(context: Context) : super(context) {

        inflateView(context)
    }

    /**
     * Creates a new instance.
     *
     * @param context Use for inflation.
     * @param attributeSet Used for custom attributes.
     */
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {

        inflateView(context)
    }

    /**
     * Updates the score on the view.
     *
     * @param score The score to update.
     */
    fun updateScore(score: Int) {
        _scoreProgressBar.progress = score
        _creditScoreTextView.text = score.toString()

        updateCreditScoreTextToMatchGradient(score)
    }

    /**
     * Inflates the view internally.
     *
     * @param context Used for inflation.
     */
    private fun inflateView(context: Context) {
        View.inflate(context, R.layout.component_donut_score_view, this)

        _scoreProgressBar = findViewById(R.id.pbCreditScoreDonut)
        _creditScoreTextView = findViewById(R.id.tvCreditScore)
    }

    /**
     * Updates the credit score text to match the progress bar's gradient colour.
     *
     * @param score The score to match the color for.
     */
    private fun updateCreditScoreTextToMatchGradient(score: Int) {
        // We could do something a lot nicer here, like programmatically calculating the color gradient given the
        // current score given that we know the start and end color hex values (defined in the colours resource).
        // However, for this demo we simply divide the scores up into five buckets.
        val textColor: Int
        val interval = MAX_CREDIT_SCORE / 5;

        when {
            score < interval -> textColor = ContextCompat.getColor(context, R.color.colorInnerRingStart)
            score < interval * 2 -> textColor = ContextCompat.getColor(context, R.color.colorInnerRingFirstQuarter)
            score < interval * 3 -> textColor = ContextCompat.getColor(context, R.color.colorInnerRingCenter)
            score < interval * 4 -> textColor = ContextCompat.getColor(context, R.color.colorInnerRingLastQuatrer)
            score <= MAX_CREDIT_SCORE -> textColor = ContextCompat.getColor(context, R.color.colorInnerRingEnd)
            else -> textColor = ContextCompat.getColor(context, R.color.colorOuterRing)
        }

        _creditScoreTextView.setTextColor(textColor);
    }

}
