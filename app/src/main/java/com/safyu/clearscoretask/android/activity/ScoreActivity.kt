package com.safyu.clearscoretask.android.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.safyu.clearscoretask.R

/**
 * Activity for displaying a Credit Score.
 *
 * Created by Safyu on 09/02/2018.
 */
class ScoreActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score)
    }
}
