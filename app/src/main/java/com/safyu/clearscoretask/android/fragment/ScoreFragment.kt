package com.safyu.clearscoretask.android.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import com.safyu.clearscoretask.R
import com.safyu.clearscoretask.android.ClearScoreTaskApplication
import com.safyu.clearscoretask.android.fragment.component.DonutScoreView
import com.safyu.clearscoretask.presenter.ScorePresenter
import com.safyu.clearscoretask.views.ScoreView

/**
 * Fragment for displaying the donut credit score.
 *
 * Created by Safyu on 09/02/2018.
 */
class ScoreFragment : Fragment(), ScoreView, View.OnClickListener {

    /** Instance of the presenter associated with this view. */
    private lateinit var _scorePresenter : ScorePresenter

    /** Layout holding the various error view components. */
    private lateinit var _errorLayout : RelativeLayout

    /** Layout holding the various loading view components. */
    private lateinit var _loadingLayout : RelativeLayout

    /** Layout holding the donut view. */
    private lateinit var _scoreLayout : DonutScoreView

    /** View holding the text for an error. */
    private lateinit var _errorMessageTextView : TextView

    /** The button for retrying a network call. */
    private lateinit var _retryButton : Button

    /** Represents the current credit score. A value of -1 denotes that a score has not yet been retrieved from the
     * server. */
    private var _score = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        val application = activity.application as ClearScoreTaskApplication
        _scorePresenter = application.getPresenterFactory().scorePresenter;

        // Retain instance so that variables aren't cleared on screen orientation.
        retainInstance = true

        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inflatedView = inflater.inflate(R.layout.fragment_credit_score, container)

        findAndAssignViews(inflatedView)

         _scorePresenter.handleLaunch(_score, this);

        return inflatedView
    }

    override fun onDestroy() {
        _scorePresenter.handleOnDestroy();

        super.onDestroy()
    }

    override fun updateScore(score: Int) {
        _score = score

        // Ensure all other views are gone
        _loadingLayout.visibility = View.GONE
        _errorLayout.visibility = View.GONE

        _scoreLayout.visibility = View.VISIBLE
        _scoreLayout.updateScore(score)
    }

    override fun displayError() {
        _loadingLayout.visibility = View.GONE
        _errorLayout.visibility = View.VISIBLE

        // Since the fragment can get attached, we need to null check the below
        _errorMessageTextView.text = activity?.applicationContext?.resources?.getString(R.string.error_generic_retry);
    }

    override fun displayNotConnectedError() {
        _loadingLayout.visibility = View.GONE
        _errorLayout.visibility = View.VISIBLE

        // Since the fragment can get attached, we need to null check the below
        _errorMessageTextView.text = activity?.applicationContext?.resources?.getString(R.string.error_not_connected);
    }

    override fun showLoading() {
        // Ensure the other views are gone
        _errorLayout.visibility = View.GONE
        _scoreLayout.visibility = View.GONE

        _loadingLayout.visibility = View.VISIBLE
    }

    override fun onClick(view: View?) {
        if (view?.id == R.id.btnRetry) {
            _scorePresenter.handleScoreRetry(this);
        }
    }

    /**
     * Finds and assigns the views associated with the fragment.
     *
     * @param inflatedView The view containing the views to find.
     */
    private fun findAndAssignViews(inflatedView: View) {
        _errorLayout = inflatedView.findViewById(R.id.rlError);
        _loadingLayout = inflatedView.findViewById(R.id.rlLoadingLayout)
        _scoreLayout = inflatedView.findViewById(R.id.dsvDonutScoreView)

        _errorMessageTextView = inflatedView.findViewById(R.id.tvError)

        _retryButton = inflatedView.findViewById(R.id.btnRetry)
        _retryButton.setOnClickListener(this)
    }
}
