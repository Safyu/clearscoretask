package com.safyu.clearscoretask.android

import android.app.Application
import com.safyu.clearscoretask.R
import com.safyu.clearscoretask.factory.InteractorFactory
import com.safyu.clearscoretask.factory.NetworkServiceFactory
import com.safyu.clearscoretask.factory.PresenterFactory

/**
 * Application class.
 *
 * Created by Safyu on 09/02/2018.
 */
open class ClearScoreTaskApplication : Application() {

    /** Instance of the factory for providing presenter instances. */
    private lateinit var _presenterFactory : PresenterFactory

    override fun onCreate() {
        super.onCreate()

        // We obtain the URL for the service from resources, which has been defined in the app's build.gradle from
        // a value in gradle.properties. This is done so that the URL can easily be changed by both developers and CI.
        val clearScoreServiceUrl = resources.getString(R.string.clear_score_service_url)

        val networkServiceFactory = NetworkServiceFactory(clearScoreServiceUrl)
        val interactorFactory = InteractorFactory(networkServiceFactory)
        _presenterFactory = PresenterFactory(interactorFactory, applicationContext)
    }

    /**
     * Gets an instance of the presenter factory for providing instances of presenters that contain the business logic
     * for views.
     *
     * @return An instance of the presenter factory.
     */
    open fun getPresenterFactory(): PresenterFactory {
        return _presenterFactory
    }
}
