package com.safyu.clearscoretask.factory

import android.content.Context
import com.safyu.clearscoretask.interactor.ScoreInteractor
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import junit.framework.Assert.assertSame
import org.junit.Before
import org.junit.Test

/**
 * Tests for [PresenterFactory].
 *
 * Created by Safyu on 09/02/2018.
 */
class PresenterFactoryTest {

    /** Instance of the System Under Test. */
    private lateinit var sut : PresenterFactory;

    @MockK
    private lateinit var mockContext : Context

    @MockK
    private lateinit var mockInteractorFactory: InteractorFactory

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        every {
            mockInteractorFactory.scoreInteractor
        } answers {
            mockk<ScoreInteractor>(relaxed = true)
        }

        sut = PresenterFactory(mockInteractorFactory, mockContext);
    }

    @Test
    fun getScorePresenter_returnsSameInstance() {
        val scorePresenterOne = sut.scorePresenter;
        val scorePresenterTwo = sut.scorePresenter;

        assertSame(scorePresenterOne, scorePresenterTwo);
    }
}


