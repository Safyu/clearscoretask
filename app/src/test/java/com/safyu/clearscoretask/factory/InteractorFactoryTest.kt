package com.safyu.clearscoretask.factory

import com.safyu.clearscoretask.interactor.ScoreInteractor
import com.safyu.clearscoretask.networking.ClearScoreService
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import junit.framework.Assert.assertSame
import org.junit.Test

import org.junit.Before

/**
 * Tests for [InteractorFactory].
 *
 * Created by Safyu on 09/02/2018.
 */
class InteractorFactoryTest {

    /** Instance of the System Under Test. */
    private lateinit var sut : InteractorFactory;

    @MockK
    private lateinit var mockNetworkServiceFactory : NetworkServiceFactory;

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        every {
            mockNetworkServiceFactory.clearScoreService
        } answers {
            mockk<ClearScoreService>(relaxed = true)
        }

        sut = InteractorFactory(mockNetworkServiceFactory)
    }

    @Test
    fun getScoreInteractor_returnsSameInstance() {
        val scoreInteractorOne = sut.scoreInteractor;
        val scoreInteractorTwo = sut.scoreInteractor;

        assertSame(scoreInteractorOne, scoreInteractorTwo)
    }

}