package com.safyu.clearscoretask.factory

import junit.framework.Assert.assertSame
import org.junit.Before
import org.junit.Test

/**
 * Tests for [NetworkServiceFactory].
 *
 * Created by Safyu on 10/02/2018.
 */
class NetworkServiceFactoryTest {

    /** Instance of the System Under Test. */
    private lateinit var sut : NetworkServiceFactory;

    @Before
    fun setUp() {
        sut = NetworkServiceFactory("http://www.test.com");
    }

    @Test
    fun getClearScoreService_returnsSameInstance() {
        val clearScoreServiceOne = sut.clearScoreService;
        val clearScoreServiceTwo = sut.clearScoreService;

        assertSame(clearScoreServiceOne, clearScoreServiceTwo);
    }

}