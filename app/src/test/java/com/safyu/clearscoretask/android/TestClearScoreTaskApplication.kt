package com.safyu.clearscoretask.android

import com.safyu.clearscoretask.factory.PresenterFactory
import io.mockk.mockk

/**
 * Test Application class for providing mocks to Robolectric Tests.
 *
 * Created by Safyu on 11/02/2018.
 */
class TestClearScoreTaskApplication : ClearScoreTaskApplication() {

    /** Factory for providing presenter instances. */
    private var _presenterFactory : PresenterFactory? = null

    override fun getPresenterFactory(): PresenterFactory {
        if ( _presenterFactory == null) {
            _presenterFactory = mockk<PresenterFactory>(relaxed = true)
        }
        return _presenterFactory as PresenterFactory
    }

}