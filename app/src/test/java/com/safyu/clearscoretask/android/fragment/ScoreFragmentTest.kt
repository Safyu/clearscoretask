package com.safyu.clearscoretask.android.fragment

import android.view.View
import android.widget.TextView
import com.safyu.clearscoretask.BuildConfig
import com.safyu.clearscoretask.R
import com.safyu.clearscoretask.android.TestClearScoreTaskApplication
import com.safyu.clearscoretask.factory.PresenterFactory
import com.safyu.clearscoretask.presenter.ScorePresenter
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil.startFragment

/**
 * Tests for [ScoreFragment].
 *
 * Created by Safyu on 11/02/2018.
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, application = TestClearScoreTaskApplication::class)
class ScoreFragmentTest {

    private lateinit var sut : ScoreFragment

    @MockK
    private lateinit var mockScorePresenter : ScorePresenter

    @MockK
    private lateinit var mockPresenterFactory : PresenterFactory

    @MockK
    private lateinit var mockView: View

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        mockPresenterFactory = (RuntimeEnvironment.application as TestClearScoreTaskApplication).getPresenterFactory()
        every {
            mockPresenterFactory.scorePresenter
        } answers {
            mockScorePresenter
        }

        every {
            mockScorePresenter.handleLaunch(any(), any())
        } just Runs

        sut = ScoreFragment()
        startFragment(sut)
    }

    @Test
    fun onCreate_callsPresenterToHandleLaunch() {
        verify { mockScorePresenter.handleLaunch(-1, sut) }
    }

    @Test
    fun onDestory_callsPresenterToHandleDestroy() {
        every {
            mockScorePresenter.handleOnDestroy()
        } just Runs

        sut.onDestroy()

        verify { mockScorePresenter.handleOnDestroy() }
    }

    @Test
    fun updateScore_showsViewWithScore() {
        var score = 500

        sut.updateScore(score)

        assertViewVisibility(R.id.rlError, View.GONE)
        assertViewVisibility(R.id.dsvDonutScoreView, View.VISIBLE)
        assertViewVisibility(R.id.rlLoadingLayout, View.GONE)

        assertEquals(score.toString(), sut.view?.findViewById<TextView>(R.id.tvCreditScore)?.text)
    }

    @Test
    fun displayError_displaysGenericError() {
        setViewVisibility(R.id.rlError, View.GONE)
        setViewVisibility(R.id.rlLoadingLayout, View.VISIBLE)

        sut.displayError()

        assertViewVisibility(R.id.rlError, View.VISIBLE)
        assertViewVisibility(R.id.rlLoadingLayout, View.GONE)

        assertEquals(RuntimeEnvironment.application.resources.getString(R.string.error_generic_retry),
                sut.view?.findViewById<TextView>(R.id.tvError)?.text)
    }

    @Test
    fun displayError_displayNotConnectedError() {
        setViewVisibility(R.id.rlError, View.GONE)
        setViewVisibility(R.id.rlLoadingLayout, View.VISIBLE)

        sut.displayNotConnectedError()

        assertViewVisibility(R.id.rlError, View.VISIBLE)
        assertViewVisibility(R.id.rlLoadingLayout, View.GONE)

        assertEquals(RuntimeEnvironment.application.resources.getString(R.string.error_not_connected),
                sut.view?.findViewById<TextView>(R.id.tvError)?.text)
    }

    @Test
    fun showLoading_showsLoadingViewHidesOthers() {
        // Setup the views to be in a contrary state
        setViewVisibility(R.id.rlError, View.VISIBLE)
        setViewVisibility(R.id.dsvDonutScoreView, View.VISIBLE)
        setViewVisibility(R.id.rlLoadingLayout, View.GONE)

        sut.showLoading()

        assertViewVisibility(R.id.rlError, View.GONE)
        assertViewVisibility(R.id.dsvDonutScoreView, View.GONE)
        assertViewVisibility(R.id.rlLoadingLayout, View.VISIBLE)
    }

    @Test
    fun onClick_retryButtonCallsPresenter() {
        every {
            mockView.id
        } answers {
            R.id.btnRetry
        }

        every {
            mockScorePresenter.handleScoreRetry(sut)
        } just Runs

        sut.onClick(mockView)

        verify { mockScorePresenter.handleScoreRetry(sut) }
    }

    /**
     * Asserts the provided view has the correct visibility.
     *
     * @param viewId The ID of the view to check.
     * @param viewVisibility The expected visibility.
     */
    private fun assertViewVisibility(viewId: Int, viewVisibility: Int) {
        assertEquals(viewVisibility, sut.view?.findViewById<View>(viewId)?.visibility)
    }

    /**
     * Sets the visibility on the provided view.
     *
     * @param viewId The ID of the view to set the visibility of.
     * @param viewVisibility The visibility of the view to set.
     */
    private fun setViewVisibility(viewId: Int, viewVisibility: Int) {
        sut.view?.findViewById<View>(viewId)?.visibility = viewVisibility
    }
}


