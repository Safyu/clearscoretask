package com.safyu.clearscoretask.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test

/**
 * Tests for [NetworkConnectivityChecker]
 *
 * Created by Safyu on 11/02/2018.
 */
class NetworkConnectivityCheckerTest {

    /** Instance of the System Under Test. */
    private lateinit var sut : NetworkConnectivityChecker

    @MockK
    private lateinit var mockContext : Context

    @MockK
    private lateinit var mockConnectivityManager : ConnectivityManager

    @MockK
    private lateinit var mockNetworkInfo : NetworkInfo

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        every {
            mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)
        } answers {
            mockConnectivityManager
        }

        sut = NetworkConnectivityChecker(mockContext)
    }

    @Test
    fun isNetworkConnected_returnsFalseIfConnectionIsNull() {
        every {
            mockConnectivityManager.activeNetworkInfo
        } answers {
            null
        }

        assertFalse(sut.isNetworkConnected())
    }

    @Test
    fun isNetworkConnected_returnsFalseIfConnectionIsNotConnected() {
        every {
            mockConnectivityManager.activeNetworkInfo
        } answers {
            mockNetworkInfo
        }

        every {
            mockNetworkInfo.isConnectedOrConnecting
        } answers {
            false
        }

        assertFalse(sut.isNetworkConnected())
    }

    @Test
    fun isNetworkConnected_returnsTrueIfConnectionIsConnected() {
        every {
            mockConnectivityManager.activeNetworkInfo
        } answers {
            mockNetworkInfo
        }

        every {
            mockNetworkInfo.isConnectedOrConnecting
        } answers {
            true
        }

        assertTrue(sut.isNetworkConnected())
    }
}