package com.safyu.clearscoretask.presenter

import com.safyu.clearscoretask.interactor.ScoreInteractor
import com.safyu.clearscoretask.util.NetworkConnectivityChecker
import com.safyu.clearscoretask.views.ScoreView
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test

/**
 * Tests for [ScorePresenter].
 *
 * Created by Safyu on 10/02/2018.
 */
class ScorePresenterTest {

    /** Instance of the System Under Test. */
    private lateinit var sut : ScorePresenter

    @MockK
    private lateinit var mockView : ScoreView

    @MockK
    private lateinit var mockNetworkConnectivityChecker : NetworkConnectivityChecker

    private lateinit var mockInteractor : ScoreInteractor

    private var creditScoreNotYetObtained = -1

    private var creditScoreObtained = 50

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        every {
            mockView.showLoading()
        } just Runs

        every {
            mockView.displayNotConnectedError()
        } just Runs

        // Since we just need to ensure it is called, we set the strictness to relaxed so that exceptions aren't
        // thrown from the testing library
        mockInteractor = mockk<ScoreInteractor>(relaxed = true)

        sut = ScorePresenter(mockInteractor, mockNetworkConnectivityChecker);
    }

    @Test
    fun handleLaunch_callsInteractorToPerformScoreLookupIfConnected() {
        provideNetworkAvailabilityForTestCase(true)

        sut.handleLaunch(creditScoreNotYetObtained, mockView)

        verifyNetworkingCallIsPerformed()
    }

    @Test
    fun handleLaunch_doesNotCallInteractorIfCreditScoreAlreadyObtained() {
        provideNetworkAvailabilityForTestCase(true)

        every {
            mockInteractor.performScoreLookup(any(), any())
        } throws Throwable("Interactor should not be called")

        every {
            mockView.updateScore(creditScoreObtained)
        } just Runs

        sut.handleLaunch(creditScoreObtained, mockView)

        verify { mockView.updateScore(creditScoreObtained) }
    }

    @Test
    fun handleLaunch_doesNotCallInteractorIfNotConnected() {
        provideNetworkAvailabilityForTestCase(false)

        every {
            mockInteractor.performScoreLookup(any(), any())
        } throws Throwable("Interactor should not be called")

        sut.handleLaunch(creditScoreNotYetObtained, mockView)

        verify { mockNetworkConnectivityChecker.isNetworkConnected() }
    }

    @Test
    fun handleLaunch_callsViewToDisplayErrorIfNotConnected() {
        provideNetworkAvailabilityForTestCase(false)

        sut.handleLaunch(creditScoreNotYetObtained, mockView)

        verify { mockNetworkConnectivityChecker.isNetworkConnected() }
        verify { mockView.displayNotConnectedError() }
    }

    @Test
    fun handleOnDestroy_callsInteractor() {
        sut.handleOnDestroy()

        verify { mockInteractor.removeObservers() }
    }

    @Test
    fun handleScoreRetry_callsInteractorToPerformScoreLookupIfConnected() {
        provideNetworkAvailabilityForTestCase(true)

        sut.handleScoreRetry(mockView)

        verifyNetworkingCallIsPerformed()
    }

    @Test
    fun handleScoreRetry_callsViewToShowLoading() {
        provideNetworkAvailabilityForTestCase(true)

        sut.handleScoreRetry(mockView)

        verify { mockView.showLoading() }
    }

    /**
     * Sets up the Network Connectivity Checker mock to return the value based on the
     * provided value.
     *
     * @param isInternetConnected True if the network should be available, else false.
     */
    private fun provideNetworkAvailabilityForTestCase(isInternetConnected: Boolean) {
        every {
            mockNetworkConnectivityChecker.isNetworkConnected()
        } answers {
            isInternetConnected
        }
    }

    /**
     * Verifies the network call is performed.
     */
    private fun verifyNetworkingCallIsPerformed() {
        verify { mockNetworkConnectivityChecker.isNetworkConnected() }
        verify { mockInteractor.performScoreLookup(any(), any()) }
    }
}
